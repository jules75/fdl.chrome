
let courseTable = document.querySelector("form table + table + table");
let assessmentsTable = document.querySelector("form table + table");
let gradesTable = document.querySelector("form table:first-of-type");

var buttonAssessments, buttonCourse, buttonGrade;

function createButtons() {
    var div = document.createElement("div");
    div.id = "tabs";

    buttonAssessments = document.createElement("button");
    buttonAssessments.innerText = "Assessments";

    buttonCourse = document.createElement("button");
    buttonCourse.innerText = "Course";

    buttonGrade = document.createElement("button");
    buttonGrade.innerText = "Grade summary";

    buttonMoodle = document.createElement("button");
    buttonMoodle.innerText = hasMoodleLink() ? "Moodle" : "Set Moodle link";

    div.appendChild(buttonCourse);
    div.appendChild(buttonAssessments);
    div.appendChild(buttonGrade);
    div.appendChild(buttonMoodle);

    document.body.insertBefore(div, document.forms['frmHeader']);

    buttonAssessments.addEventListener("click", onButtonAssessmentsClick);
    buttonCourse.addEventListener("click", onButtonCourseClick);
    buttonGrade.addEventListener("click", onButtonGradeClick);
    buttonMoodle.addEventListener("click", onButtonMoodleClick);
}

function hide(el) {
    el.style.display = 'none';
}
function show(el) {
    el.style.display = 'table';
}

// We can't just hide the grades table, as we lose the
// help/recalc/save buttons as well
function hideSpecial(el) {
    el.style.position = 'fixed';
    el.style.top = '-99999px';
}

function showSpecial(el) {
    el.style.position = 'static';
}

function onButtonAssessmentsClick(e) {
    buttonAssessments.classList.add('active');
    buttonCourse.classList.remove('active');
    buttonGrade.classList.remove('active');
    show(assessmentsTable);
    hide(courseTable);
    hideSpecial(gradesTable);
}

function onButtonCourseClick(e) {
    buttonAssessments.classList.remove('active');
    buttonCourse.classList.add('active');
    buttonGrade.classList.remove('active');
    hide(assessmentsTable);
    show(courseTable);
    hideSpecial(gradesTable);
}

function onButtonGradeClick(e) {
    buttonAssessments.classList.remove('active');
    buttonCourse.classList.remove('active');
    buttonGrade.classList.add('active');
    hide(assessmentsTable);
    hide(courseTable);
    showSpecial(gradesTable);
}

function getMoodleLinks() {
    return JSON.parse(localStorage.getItem("moodleLinks"));
}

function hasMoodleLink() {
    let course = getCurrentCourse();
    return _.has(getMoodleLinks(), [course.locationId, course.termId, course.courseId]);
}

function forgetMoodleLink() {
    let course = getCurrentCourse();
    var moodleLinks = getMoodleLinks();
    _.unset(moodleLinks, [course.locationId, course.termId, course.courseId]);
    localStorage.setItem("moodleLinks", JSON.stringify(moodleLinks));
}

function onButtonMoodleClick(e) {

    let course = getCurrentCourse();

    // check storage
    var moodleLinks = getMoodleLinks();

    // initialise if needed
    if (moodleLinks == null) {
        moodleLinks = {};
    }

    // open new tab (if moodle link exists)
    let url = _.get(moodleLinks, [course.locationId, course.termId, course.courseId]);
    if (url) {
        window.open(url,'_blank');
        return;
    }

    // prompt user for link, save to storage
    let link = prompt("Enter full URL (web address) of Moodle shell for this course");
    if (link) {
        _.set(moodleLinks, [course.locationId, course.termId, course.courseId], link);
        localStorage.setItem("moodleLinks", JSON.stringify(moodleLinks));
        window.open(link,'_blank');
    }
}

/**
 * Moving help/recalc/save buttons (see header.css) buggers up the
 * grades table. This fn adds a td to make it display correctly again.
 */
function fixGradesTable() {
    let tr = document.querySelector("tr + tr");
    let firstTd = document.querySelector("tr + tr td:first-child");
    let newTd = document.createElement('td');
    tr.insertBefore(newTd, firstTd);
}

function getSavedCourses() {
    return JSON.parse(localStorage.getItem("savedCourses"));
}

// save course to local storage
function save(course) {

    savedCourses = getSavedCourses();
    
    // initialise if needed
    if (savedCourses == null) {
        savedCourses = [];
    }

    savedCourses.push(course);
    
    // avoid duplicates
    savedCourses = _.uniqBy(savedCourses, 
        function (e) {
            return e.locationId + e.termId + e.courseId;
        }
    );

    localStorage.setItem("savedCourses", JSON.stringify(savedCourses));
}

// remove course from local storage
function unsave(course) {
    savedCourses = _.reject(getSavedCourses(), course);
    localStorage.setItem("savedCourses", JSON.stringify(savedCourses));
}

function getCurrentCourse() {
    return {
        locationId: $("select[name='optlocationid'] option:selected").text(),
        termId: $("select[name='opttermid'] option:selected").text(),
        courseId: $("select[name='optunitid'] option:selected").text()
    };
}

/**
 * Changing the course dropdowns dynamically is tricky.
 * While we might have the 3 course attributes ready (location, term, course id),
 * the term and course IDs might not be present in the current page's dropdowns.
 * 
 * Here's what we need to do;
 * 
 *  1. Save the course we want to load to localStorage as 'loadCourse'
 *  2. Change the location dropdown, then trigger a reload
 *  3. Change the term dropdown, trigger another reload
 *  4. Change the course id, trigger the final reload
 *  5. Delete the 'loadCourse' entry from localStorage
 * 
 */
function checkLoadCourse() {

    courseEntry = JSON.parse(localStorage.getItem("loadCourse"));

    if (courseEntry !== null) {

        // hide everything while loading
        document.body.style.display = 'none';

        if (courseEntry.locationId) {
            $("select[name='optlocationid']").val(courseEntry.locationId);
            delete courseEntry.locationId;
            localStorage.setItem("loadCourse", JSON.stringify(courseEntry));
        }

        else if (courseEntry.termId) {
            $("select[name='opttermid']").val(courseEntry.termId);
            delete courseEntry.termId;
            localStorage.setItem("loadCourse", JSON.stringify(courseEntry));
        }

        else if (courseEntry.courseId) {
            $("select[name='optunitid']").val(courseEntry.courseId);
            delete courseEntry.courseId;
            localStorage.removeItem("loadCourse");
        }        

        document.forms[0].btnGo.click();
        return;
    }
}

function createFavouriteLink() {
    
    let starOpen = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QgYAioXWy3LtwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAEUSURBVCjPndI7L8RREAXwn0c0K2s9IiiIEBoUCIVGQiR0NCJKj9onUKl8BY2WKDwqnUKxlUZiIxKdIEKxCY0EzWzyt9ZrT3UzM+fcOedefsaEMjGDd/SVQ87iEsf/Jc7jCoPIY+wvpDaM4AJrUdvEGQbQmhyuwGr4a0M9nnGHJdygBQeoRhpPuMc2jEcwu2hHBxpDuIDm6PViDy8Yhaq4+QHLv9haiW2mi8TN4RaLJUiVWIj+3HfK6ziMtZNowBE2ihWT4aXjiR5Rg56YySOHTJJcnTjXR7JZ9If/YZxGmJeYDbE3RaY7sYWmSDOHkwinC69BWojQPmEI19jBJFJRr8MU9nEen+gLUugO36WQie1qC4UPziozQv9a80oAAAAASUVORK5CYII=";
    let starClosed = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QgYAjQq1wS4eQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAE8SURBVCjPjdIxSNVRFMfxz7sZRAgKttQLgtwFR5f/EBFdoR44tTW2RISLu4M4CeLYVOBUNLT8l1ou2NYabg2C4KCIiov4fy73b5fX648HLueee+/3HO45P0YsxVDun+uw8L+LFMMAdYphfjTpWDjFoKqbNlzJfh3l+V+4zNg+SDG8wlw+fppiqMZ9r1cEAQ/xABtYyFdD/MRbHGKvLdJLMbzDM0xjMvs+JopCDf7gFGfZf5rALjZ1W8BsEV9gLeAHXuTsN7ETDLDTK8awhG3c6QCP8Kaqmy/X3a7qRlU3X7HaAQ6x1YIpBqEYzy3c7YB7uaHXYy1FMo3HI8DlSNxPMdwep7CpAj7HB7zGx9xduJ/XP/A9PMJnvMRyVTfbeI9FfMsC6rdAKYTfeIL9qm5OCq0f43uK4RdmcNACVykCWXbN/zCDAAAAAElFTkSuQmCC";    
    let img = $('<img />');
    let courseIsSaved = _.some(getSavedCourses(), getCurrentCourse());
    
    $(img).attr('src', courseIsSaved ? starClosed : starOpen);
    $(img).attr('title', courseIsSaved ? "Remove from favourites" : "Add to favourites");
    $('body > form > table:nth-child(4) > tbody > tr > td:nth-child(1) > table > tbody > tr:nth-child(1) > td').append(img);
    
    $(img).click(function(e) {
        if (courseIsSaved) {
            unsave(getCurrentCourse());
        }
        else {
            save(getCurrentCourse());
        }
        parent.location.reload();
    });

}

createButtons();
onButtonCourseClick();
fixGradesTable();
checkLoadCourse();
createFavouriteLink();
