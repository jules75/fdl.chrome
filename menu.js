
// replace hearts with stars
let starOpen = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QgYAioXWy3LtwAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAEUSURBVCjPndI7L8RREAXwn0c0K2s9IiiIEBoUCIVGQiR0NCJKj9onUKl8BY2WKDwqnUKxlUZiIxKdIEKxCY0EzWzyt9ZrT3UzM+fcOedefsaEMjGDd/SVQ87iEsf/Jc7jCoPIY+wvpDaM4AJrUdvEGQbQmhyuwGr4a0M9nnGHJdygBQeoRhpPuMc2jEcwu2hHBxpDuIDm6PViDy8Yhaq4+QHLv9haiW2mi8TN4RaLJUiVWIj+3HfK6ziMtZNowBE2ihWT4aXjiR5Rg56YySOHTJJcnTjXR7JZ9If/YZxGmJeYDbE3RaY7sYWmSDOHkwinC69BWojQPmEI19jBJFJRr8MU9nEen+gLUugO36WQie1qC4UPziozQv9a80oAAAAASUVORK5CYII=";
let starClosed = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QgYAjQq1wS4eQAAABl0RVh0Q29tbWVudABDcmVhdGVkIHdpdGggR0lNUFeBDhcAAAE8SURBVCjPjdIxSNVRFMfxz7sZRAgKttQLgtwFR5f/EBFdoR44tTW2RISLu4M4CeLYVOBUNLT8l1ou2NYabg2C4KCIiov4fy73b5fX648HLueee+/3HO45P0YsxVDun+uw8L+LFMMAdYphfjTpWDjFoKqbNlzJfh3l+V+4zNg+SDG8wlw+fppiqMZ9r1cEAQ/xABtYyFdD/MRbHGKvLdJLMbzDM0xjMvs+JopCDf7gFGfZf5rALjZ1W8BsEV9gLeAHXuTsN7ETDLDTK8awhG3c6QCP8Kaqmy/X3a7qRlU3X7HaAQ6x1YIpBqEYzy3c7YB7uaHXYy1FMo3HI8DlSNxPMdwep7CpAj7HB7zGx9xduJ/XP/A9PMJnvMRyVTfbeI9FfMsC6rdAKYTfeIL9qm5OCq0f43uK4RdmcNACVykCWXbN/zCDAAAAAElFTkSuQmCC";
document.querySelectorAll("img[src='image/heart.gif']").forEach(e => e.setAttribute('src', starOpen));
document.querySelectorAll("img[src='image/heart.png']").forEach(e => e.setAttribute('src', starClosed));

// make menu headings clickable
let tabs = document.querySelectorAll("div#menutab1");
tabs.forEach(function(tab) {
    tab.addEventListener("click", function(e) {
        let link = e.target.getElementsByTagName('a')[0];
        link.click();
    })
})


function createSavedCoursesDisplay() {
    
    let courses = JSON.parse(localStorage.getItem("savedCourses"));
    let groupedCourses = _.groupBy(courses, function(c) { return c.courseId + ' ' + c.termId; });

    let ul = $("<ul id='saved'></ul>");

    function createItem(course) {
        
        let li = $(`<li class="courselink">${course.locationId}</li>`);
        $(ul).append(li);

        // on item click, save to local storage and refresh
        $(li).click(function(c) {
            localStorage.setItem("loadCourse", JSON.stringify(course));
            parent.location.reload();
        })
    }

    function createItemGroup(courses, key) {
        let li = $(`<li>${key}</li>`);
         $(ul).append(li);
        _.sortBy(courses, 'locationId').map(createItem);
    }    

    _.each(groupedCourses, createItemGroup);
    $('body').append(ul);
}

createSavedCoursesDisplay();
